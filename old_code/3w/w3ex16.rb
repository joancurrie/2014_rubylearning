#array:

a = ["Magazine", "Sunday", "Jump"]
#1.TRUE: jec = a.sort
#2.false: jec = a.sort { |s| s }
#3.TRUE: jec = a.sort { |l, r| l <=> r }
#4.false: jec = a.sort { |l, r| l.length <=> r.length }
#5.TRUE: jec = a.sort_by { |s| s }
#6.false: jec = a.sort_by { |s| s.length } 

puts jec