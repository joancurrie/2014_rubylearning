filename = 'text.txt'
input_file = File.open(filename).read

output_information = input_file.gsub('word', 'inserted word')
File.open(filename, 'w') do |content|
  content.write(output_information)
end

my_output = DATA.read.gsub('word', 'inserted word')
puts my_output

puts "Second run with Data"
DATA.rewind  # Without the rewind, it will stay at the end of THIS file.
my_output = DATA.read.gsub('inserted', '')
puts my_output.inspect
__END__
text text text text text
text text text text text
text text word text text
text text text text text
text text text text text
