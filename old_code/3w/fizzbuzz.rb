#w3ex8:
1.upto(100) do |x|
  if x % 3 == 0 and x % 5 == 0
    print 'FizzBuzz'  
  elsif x % 3 == 0  # i = 3,6,9,12,18,...
    print 'Fizz'
  elsif x % 5 == 0
    print 'Buzz'  #i = 5,10,20,25,...
  else 
    print x  #i = 1,2,4,7,8,
  end
  puts '' 
end