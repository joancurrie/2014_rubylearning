collection = [12, 23, 456, 123, 4579]

collection.each do |n|
 puts n.even? ? "#{n}: even" : "#{n}: odd"
end