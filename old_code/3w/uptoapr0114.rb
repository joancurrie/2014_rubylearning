# Go up from 3 to 5.
3.upto(5) do |x|

    # Go up from x to x + 2.
    x.upto(x + 2) do |y|
	# Display variable.
	print y, " "
    end

    # End the line.
    print "\n"
end