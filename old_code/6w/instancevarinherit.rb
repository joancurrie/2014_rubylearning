# p049instvarinherit.rb  
class C  
  def initialize  
    @n = 100  
  end  
  
  def increase_n  
    puts "an is #{@n}" 
    @n *= 20  
    puts "an is #{@n}" 
  end  

end  
  
class D < C  

  def show_n  
    puts "bn is #{@n}"  
  end  
end  
  
d = D.new  
d.increase_n  
d.show_n  