def id3_tag_analyzer(mp3_file, &content)
 fields = []
  File.open('rubyprograms/song.mp3') do |f|
   f.seek(-128, IO::SEEK_END)
    if f.read(3) == "TAG"
     #read 125 bytes and split the string 
     f.read(125).split(/\x00/).each do |each_field|
      fields << each_field unless each_field == "" #store each_field info in an array
     end
    end
  end
  content.call(fields)
end

id3_tag_analyzer('rubyprograms/song.mp3') do |field| 
 puts "Song Title: #{field[0]}"
 puts "Artist: #{field[1]}"
 puts "Album: #{field[2]}"
 puts "Year: #{field[3]}"
 puts "Comment: #{field[4]}"
 puts "Genre: #{field[5]}"
end
