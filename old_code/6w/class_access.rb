class Fruit
	   attr_accessor :condition
       def kind=(k)
         @kind = k
       end
       def kind
         @kind
       end
	   def inspect
         puts "a #{@condition} #{@kind}"
       end
end

f2 = Fruit.new
f2.condition = "ripe"
f2.kind = "banana"
f2.kind
f2.inspect
