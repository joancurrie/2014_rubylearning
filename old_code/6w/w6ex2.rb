#analyzes an MP3 file. 
#Many MP3 files have a 128-byte data structure at the end called an ID3 tag. 
#You can parse this data structure by opening an MP3 file 
#and doing a series of reads from a position near the end of the file.

#??work w directories: create, open, read, write to Ruby files.

#new files are **created in Ruby** using the new method of the File class.
# Create a new file and write to it  
File.open('test.rb', 'w') do |f2|  
  # use "\n" for two lines of text  
  f2.puts "Created by Satish\nThank God!"  
end  