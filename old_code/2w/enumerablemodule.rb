#[1, 2, 3].each { |number| puts number }
[1, 2, 3].select { |number| number % 1 == 0 } # => true
#[1, 3, 5].any? { |number| number % 2 == 0 } # => false