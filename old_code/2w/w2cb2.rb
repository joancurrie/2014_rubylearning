def simple
	puts 'Here comes the code block!'
	yield
	puts 'There was the code block!'
end
simple { puts 'Hooray! The code block is here!' }