# Challenge 3 from week 2
s = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.\nDuis imperdiet sem eu quam.\nDonec bibendum tincidunt purus.\nNunc eu tellus sed turpis volutpat pellentesque.\n"\
    "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.\nDuis imperdiet sem eu quam.\nDonec bibendum tincidunt purus.\nNunc eu tellus sed turpis volutpat pellentesque.\n"\
    "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.\nDuis imperdiet sem eu quam.\nDonec bibendum tincidunt purus.\nNunc eu tellus sed turpis volutpat pellentesque.\n"

def separate_strings(s, char = ' ')
  a = s.split("\n")
  array_size = a.size
  length_in_max_number = array_size.to_s.size
  array_size.times do |n|
    number = (n + 1).to_s
    formated_number = format_number(number, length_in_max_number, char)
    element = a[n]
    yield formated_number, element 
  end
end

def char_validation?(char)
  (char =='0' || char == ' ') ? true : false
end

def format_number(number, size, char = ' ')
  unless char_validation?(char)
    puts "Incorrect character, write '0' or ' ' as a second argument"
    puts "Finishing program..."
    exit
  end
  number = number.to_s
  number.size == size ? number : char * (size - number.size) + number
end

separate_strings(s, '0') do | fn, e |
  puts "Line " + fn + ': ' + e
end