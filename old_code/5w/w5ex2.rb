#spec:
#Shapes on GUI:
#a square, a circle and a triangle.
#clicks on a shape, the shape will rotate clockwise 360 degrees (ie. all the way around) 
#and play an AIF sound file specific to that particular shape. 
class Shape  
  def  initialize #(sound)  general_shape 
    puts "I am shape."
    ROTATE_CLOCKWISE = 360
	#play an AIF sound file specific to that particular shape.  @sound = sound
  end  
end  
#Inheritance class:  
#child class < parent class
class Square < Shape  
  def square_shape  
    puts "I am square shape."  
  end  
end  
class Circle < Shape  
  def circle_shape  
    puts "I am circle shape."  
  end  
end  
class Triangle < Shape  
  def triangle_shape  
    puts "I am triangle shape."  
  end 
end  
  
s = Square.new  
c = Circle.new  
t = Triangle.new  

s.square_shape
c.circle_shape
t.triangle_shape