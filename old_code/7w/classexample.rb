#Dog
class Dog
  def initialize(breed, name)
    #instance variables:
    @breed = breed
	@name = name	
  end
end 
# make an object  
# Objects are created on the heap  
d = Dog.new('Labrador', 'Benzy')
puts d.methods.sort  