# p052dumpgc.rb  
#creates ****an object of the ****above class
#and then uses ****Marshal.dump to save ****a serialized version of it to ****the disk.
require_relative 'klass'  
#gc 
obj = Klass.new(120, 'James', ['magic', 'invisibility'])  
puts "#{obj.power} #{obj.type}"  
 
puts "hello" 

obj.weapons.each do |w|  
  puts w  
  puts "hi"
end  
  
File.open('game', 'w+') do |f|  
  Marshal.dump(obj, f)  
end  