
class Rectangle
  def initialize(width, length)
    #Instance variable
    @width = width   #= 23.45 
    @length = length #= 34.67
  end
  
  # accessor method
  def area 
    area = @width*@length                  # @width * @length
    sprintf("%#g", area)	
  end
  
  def perimeter
    perimeter = 2 * ( @width + @length )    # 2 *  (@width + @length)      
    sprintf("%#g", perimeter)
  end
end

## *****************************
# make an object
  r = Rectangle.new(23.45, 34.67) 
  puts "Area is = #{r.area}" 
  puts "Perimeter is = #{r.perimeter}"



