#w4ex1:
#Classes in Ruby are first-class objects---each is an instance of class Class.
#define a class
class Dog
# *****************************
  #constructor method
  def initialize(name)  
    # Instance variables  
    @name = name  
  end  
  # define private accessor methods
  def getName
    @name
  end   
  # make them private
  private :getName

  # instance method to teach trick
  def teach_trick
    puts "#@name is dancing!"
  end
  
   # make it protected
   protected :teach_trick

 # define to_s method
   def to_s
      "(name:#@name)"  # string formatting of the object.
   end
   
  # accessor method
  def display  
    "My name is #{@name}"  
  end  
  
  def bark  
    'Ruff! Meow!'  
  end  
  
  def eat() 
    'Yummy! Yummy!'  
  end  

  def chase_cat()  
    'Hey Puss... I am going to chase you.'  
  end  
  
end
  
# *****************************
# make an object
# Objects are created on the heap.
  d = Dog.new('Leo')

  g = Dog.new('Lassie')  
  g.teach_trick(:dance)
  
  puts "d is : #{d}" 
  # use accessor methods  
  puts d.display 
  puts d.bark   
  puts d.eat
  puts d.chase_cat   
  
  

