class Dog
  
  def initialize(name = "Lucky")
    @name = name
  end

  def bark()
    "Woof woof woof !!!"
  end  

  def eat()
    "Pedigree Pall is delicious. #{@name} likes it."
  end  

  def chase_cat(cat_name = "Tom")
    "I will get you #{cat_name}"
  end  

end  

#class
d = Dog.new("Lucky") 
puts "Name is " 
puts d.bark()
