# p028swapcontents.rb

#2 text files: A & B.
#    create, open, read, write to files. 

#new method of File class
File.new("A.txt", "w")
#=> #<File:A.txt>

File.new("B.txt", "w")
#=> #<File:B.txt>

open('A.txt', 'w') { |f|
    for i in (1..5)
      puts i
      f.puts i
    end	  
}
puts "hi"
File.open('A.txt', 'r') do |f2|  
  while line = f2.gets  
    puts line  
  end  
end  
puts "joan"
open('B.txt', 'w') { |f|
    for i in (1..5)
      puts (6 - i)
      f.puts (6 - i)
    end	  
}
puts "bye"
File.open('B.txt', 'r') do |f2|  
  while line = f2.gets  
    puts line  
  end  
end  