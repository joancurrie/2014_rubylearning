#!/usr/bin/ruby -w

class Box
   # Initialize our class variables
   @@count = 0
   def initialize(w,h)
      # assign instance avriables
      @width, @height = w, h

      @@count += 1
   end
   def getWidth
      @width
   end
   def getHeight
      @height
   end

   def self.printCount()
      puts "Box count is : #@@count"
   end
end

# create two object
box1 = Box.new(10, 20)
box2 = Box.new(30, 100)

# call class method to print box count
Box.printCount()
# use accessor methods
x = box1.getWidth()
y = box1.getHeight()

puts "Width of the box is : #{x}"
puts "Height of the box is : #{y}"
x = box2.getWidth()
y = box2.getHeight()

puts "Width of the box is : #{x}"
puts "Height of the box is : #{y}"