class Rectangle
  
  def initialize(width,length)
    @width = width
    @length = length
  end  

  def area
     sprintf("%#g", (@width*@length) )
  end  

  def perimeter
     sprintf("%#g", (2 * @width + 2 * @length) )
  end  

end  
 r = Rectangle.new(23.45, 34.67) 
  puts "Area is = #{r.area}" 
  puts "Perimeter is = #{r.perimeter}"