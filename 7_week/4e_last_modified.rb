#Vic: I have put the question as "the file was last modified 125.873605469919 days ago".  I have not put the "last_modified(file)" into the method yet.

#w7e4:

#file was last modified 125.873605469919 **DAYs ago.
def last_modified()
  #file was modified many days ago.
  days_before = 125.873605469919
  #Today is :  %Y%m%d = 20140810
  t = Time.now
  print "Today is "
  puts t
  puts t.strftime("Today is %j days from the beginning of the year.")  ## 222 

  #now - days_before = final_day
  final_day = t.strftime("%j") 

  fd =  final_day.to_f - days_before.to_f
  print "365 - " + days_before.to_s + " is "
  puts fd

end

last_modified()
