def repeat(delay, times)

  limit=1
  1.upto(times){ puts "Sleep " + limit.to_s; yield; sleep delay; limit = limit + 1; } end

repeat(5.0, 12){puts "Code block is here."}

__END__
The following are comments concerning style.  I would use the upto method, but
I would use do..end rather than the {} style of the block  But it is definitely OK to use the block style you are using.  Depends on what you want to communciate, and how you want it to look.  The effect is the same.

Inspecting 1 file
W

Offenses:

7_week/5e_repeat_call.rb:2:1: C: Extra empty line detected at body beginning.
7_week/5e_repeat_call.rb:3:8: C: Surrounding space missing for operator '='.
  limit=1
       ^
7_week/5e_repeat_call.rb:4:16: C: Space missing to the left of {.
  1.upto(times){ puts "Sleep " + limit.to_b; yield; sleep delay; limit = limit + 1; } end
               ^
7_week/5e_repeat_call.rb:4:23: C: Prefer single-quoted strings when you don't need string interpolation or special symbols.
  1.upto(times){ puts "Sleep " + limit.to_b; yield; sleep delay; limit = limit + 1; } end
                      ^^^^^^^^
7_week/5e_repeat_call.rb:4:44: C: Do not use semicolons to terminate expressions.
  1.upto(times){ puts "Sleep " + limit.to_b; yield; sleep delay; limit = limit + 1; } end
                                           ^
7_week/5e_repeat_call.rb:4:66: C: Use self-assignment shorthand +=.
  1.upto(times){ puts "Sleep " + limit.to_b; yield; sleep delay; limit = limit + 1; } end
                                                                 ^^^^^^^^^^^^^^^^^
7_week/5e_repeat_call.rb:4:81: C: Line is too long. [89/80]
  1.upto(times){ puts "Sleep " + limit.to_b; yield; sleep delay; limit = limit + 1; } end
                                                                                ^^^^^^^^^
7_week/5e_repeat_call.rb:4:87: W: end at 4, 86 is not aligned with def at 1, 0
  1.upto(times){ puts "Sleep " + limit.to_b; yield; sleep delay; limit = limit + 1; } end
                                                                                      ^^^
7_week/5e_repeat_call.rb:6:16: C: Space missing to the left of {.
repeat(5.0, 12){puts "Code block is here."}
               ^
7_week/5e_repeat_call.rb:6:17: C: Space missing inside {.
repeat(5.0, 12){puts "Code block is here."}
                ^
7_week/5e_repeat_call.rb:6:22: C: Prefer single-quoted strings when you don't need string interpolation or special symbols.
repeat(5.0, 12){puts "Code block is here."}
                     ^^^^^^^^^^^^^^^^^^^^^
7_week/5e_repeat_call.rb:6:43: C: Space missing inside }.
repeat(5.0, 12){puts "Code block is here."}
                                          ^

1 file inspected, 12 offenses detected
