##lesson7exercise1.rb
#serialization MARSHALLing:
#serialize object: store them somewhere.
#              &  reconstitute objects when needed.

require_relative 'lesson7exercise1a'
class Klass
  def initialize(saying)
    # Instance variables
    @saying = saying
  end
  attr_reader :saying
  def say_hello
    puts @saying #"How do you do?"
  end
end
####-------------------------------------------
##lesson7exercise1a.rb
#creates an object of ABOVE lesson7exercise1.rb CLASS.
#DUMP:
require_relative 'lesson7exercise1'
obj = Klass.new("hello")

File.open('game', 'w+') do |f|
  Marshal.dump(obj, f)
end
File.open('game') do |f|
  @obj = Marshal.load(f)
end

puts obj.say_hello
