#w4ex1:
#Classes in Ruby are first-class objects---each is an instance of class Class.
class Dog
# *****************************
  def initialize(name)  
    # Instance variables  
    #@breed = breed  
    @name = name  
  end  

  def display  
    puts "My name is #{@name}"  
  end  
  
  def bark  
    puts 'Ruff! Meow!'  
  end  
  
  def eat() 
    puts 'Yummy! Yummy!'  
  end  
  def chase_cat()  
    puts 'Hey Puss... I am going to chase you.'  
  end  
  
end
  
# *****************************
# make an object
# Objects are created on the heap.
  d = Dog.new("Lassie")

  d.display 
  ##puts d.methods.sort    
  d.bark  
  d.eat
  d.chase_cat  
  
  
