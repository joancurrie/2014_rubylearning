#How old I am:   I am 979000000 seconds old.
=begin
                    1 sec
   1 min =         60 secs
  60 min =      60*60 secs
         =       3600 secs
  1 hour =       3600 secs
  1 day  =     86,400 secs
365 days = 365*86,400 secs
         = 31,536,000 secs
     =          1 year
=end

# doctest: seconds_to_years is a method that takes seconds and gives years
# >> seconds_to_years(979000000).round(2)
# => 31.04

def seconds_in_year
  365 * 24 * 60 * 60.0
end

def seconds_to_years(seconds)
  seconds / seconds_in_year
end

if __FILE__ == $0
  ages = {:vic => 595000001, :satish => 979000000}
  ages.each_pair do |name, seconds|
    age_in_years = seconds_to_years(seconds)
    puts "#{name.to_s.capitalize} is #{seconds} seconds old, but we would say he is #{'%0.2f' % age_in_years} years old."
  end
end

