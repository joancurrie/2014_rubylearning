#w1ex6
#Write a method called convert that takes one argument which is a temperature in degrees Fahrenheit.
#This method should return the temperature in degrees Celsius.

# doctest: Convert will convert from F to C
#          convert(-40) should be -40
# >> convert -40
# => -40
# doctest: I can add two converted values together
# >> convert(-40) + convert(32)
# => -40
# doctest: Convert name is bad for a method, because what is it converting?
#          We must have convert as a name, per specification, but we can use f_to_c
#          as a better name
# >> f_to_c(98.6)
# => 37
def convert (degrees_fahrenheit)
  (degrees_fahrenheit - 32) * 5 / 9.0
end
alias :f_to_c :convert 

boiling_point = 212
freezing = 32
body_temp  = 98.6
[boiling_point, freezing, body_temp].each do |temp|
  puts "Our temperature is %.2fF and is %.2fC." % [temp, convert(temp)]
end
