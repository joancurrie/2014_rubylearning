#What about this?:
##Overriding:
class Shape
  def initialize
    @sound = "shape sound"
  end
  def to_s
    "Shape is " + self.class.to_s + "."
  end
  def click
    puts rotate # I refer to the method, rather than the variable
    puts sound + 'aif' # This refers to the requirement of having the .aif extension
                       # not the best way to address this requirement though.
  end
  def rotate
    @rotate = "Rotate clockwise 360 degrees."
  end
  def sound
    @sound = "Sound is #{@sound}."
  end
end

class Circle < Shape
  def initialize
    super
    @sound = "circle sound"
  end
  def rotate # This is the example specifically of overriding, this is good
    @rotate = "I am not rotating, you wouldn't notice either way, anyhoo!"
  end
end
class Rectangle < Shape
  def initialize
    super
    @sound = "rectangle sound"
  end
end
class Triangle < Shape
  def initialize
    super
    @sound = "triangle sound"
  end
end

collection_of_shapes = Shape.new, Circle.new, Rectangle.new, Triangle.new

collection_of_shapes.each do |shape|
  puts shape # the .to_s call is redundant, as you know, due to your prior reading,
                  # and what you had sent in a prior post, puts to_s does
                  # generally call .to_s on the object being printed.
  shape.click
end
