some_line = ""
prompt = ">> "
result_prompt = "=> "
until some_line == "exit\n"
  print prompt
  some_line = gets   # Read
  result =  eval(some_line) # Eval
  puts result_prompt + result.inspect  #  Print
end  # Loop

# Basically 4 lines of code or close to it for four actions we
# need.
