class UnpredictableString < String
  def initialize(string)
    @original_string = string
  end
  def scramble
    @original_string.split(//).shuffle.join('')
  end
end

my_unpredictable = UnpredictableString.new('It was a dark and stormy night.')
p my_unpredictable.scramble

# >> "dtkh.o ma sa  tawrtdnsi yagrnI "
